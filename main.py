import os

import src.get_data as get_data
import src.read_data as read_data
# import src.text_preprocessing as text_preprocessing
# import src.text_classification as text_classification
import parsing.parser_file as parser_file
import json




if __name__ == '__main__':
    # get_data.main()
    # read_data.main()
    # text_preprocessing.main()
    # best_current_acc, best_previous_acc = text_classification.main()

    current_setup = parser_file.get_setup('src')

    with open('src/output/best_accuracy.json', 'r') as f:
        data = json.load(f)
        best_acc = data['accuracy']

    branch = {}
    branch["setup"] = current_setup
    branch["score"] = best_acc

    print(current_setup)
    print(branch)

    file_path = "src/output/tree_data.json"
    if os.path.exists(file_path):
        with open("src/output/tree_data.json", "r") as read_file:
            content = json.load(read_file)

    else:
        content = {}
        content["tree"] = []

    content["tree"].append(branch)

    with open("src/output/tree_data.json", "w") as write_file:
        json.dump(content, write_file, indent=2)


