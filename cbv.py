import os.path

import argparse
from shutil import move
import json

##This file contains the prototype of the CBV method.

def is_identical(path, content):
    '''
    Checks if the content given is identical to the content of the file given.

    :param path: path to the file that needs to be checked
    :param content: content for the comparison 
    :return: True if the content and the content of the file are identical, False otherwise
    '''
    check = True
    with open(path, "r") as python_file:
        for l, c in zip(python_file, content):
            if l != c:
                check = False
        python_file.close()
        return check


def write(file_name, category, name, content):
    '''
    Writes the content to a file, with the specified category and instance name 

    :param file_name: the name of the file where content was extracted form
    :param category: The category name
    :param name: The instance name
    :param content: The content that needs to be saved.
    :return: None
    '''
    dir = "structure/" +  category + "/" + name
    path = dir + "/" + file_name

    if os.path.isfile(path):
        if not is_identical(path, content):
            raise Exception("You changed the content of existing file", path)

    if not os.path.exists('structure'):
        os.mkdir('structure')
    if not os.path.exists("structure/" + category):
        os.mkdir("structure/" + category)
    if not os.path.exists(dir):
        os.mkdir(dir)
    python_file = open(path, "w")
    for line in content:
        python_file.write(line)
    python_file.close()



def find_section(file_path, component, tag_name, saved_path):
    '''
    Replaces a section the content of the file with the content of the 'saved_path' file.

    :param file_path: The file that gets updated
    :param component: The category name that is used
    :param tag_name: The instance name that is used
    :param saved_path: The file with the content
    :return: None
    '''
    endkey = "###END###"

    temp_path = file_path.rsplit('.', 1)[0] + "temp.py"

    with open(temp_path, "w") as temp_file:

        with open(file_path, "r") as python_file:
            current_line = python_file.readline()

            beginflag = False
            nameflag = False
            category = None

            line = 1
            wrote = False

            while current_line:
                if len(current_line) > 2 and current_line[
                                             :3] == "###":  # TODO script doesn't work if there is a space before the #
                    # This line is a keyword
                    if current_line.rstrip() == endkey: # End key word
                        if beginflag and nameflag:
                            temp_file.write(current_line)
                            beginflag = False
                            nameflag = False
                            category = None
                            wrote = False
                        else:
                            raise Exception("Missing ###BEGIN###", str(line)) # End key word without seeing begin or name flag
                    else:

                        temp_current_line = current_line.replace("###", "")
                        if "Name:" in temp_current_line: # current line contains name tag
                            if beginflag and not nameflag:
                                nameflag = True
                                if component == category:
                                    temp_file.write("###Name: " + tag_name + "\n")
                                else:
                                    temp_file.write(current_line)
                            else:
                                raise Exception("Missing ###BEGIN###", str(line)) # Didn't saw begin tag but saw name tag
                        else:
                            if beginflag or nameflag:
                                raise Exception("Already saw ###BEGIN###", str(line))
                            else:
                                if temp_current_line == "None":
                                    raise Exception("Cannot use None as category name", str(line))
                                category = temp_current_line.rstrip()
                                temp_file.write(current_line)
                                beginflag = True
                else:
                    if beginflag ^ nameflag:
                        raise Exception("Cannot parse content, check keywords", str(line))

                    elif beginflag and nameflag and category == component and not wrote:
                        start = line
                        with open(saved_path, "r") as saved_file:
                            new_line = saved_file.readline()
                            while new_line:
                                temp_file.write(new_line)
                                new_line = saved_file.readline()

                        wrote = True

                    elif not wrote:
                        temp_file.write(current_line)

                line += 1
                current_line = python_file.readline()


    move(temp_path, file_path)


def parse(file_path, create_structure = True, current_setup = {}):
    '''
    Saves versioned blocks of the file separately.

    :param file_path: The file for with the versioning is done
    :param create_structure: When True the blocks are saved, When False only the type of blocks are returned
    :param current_setup: The current blocks in other files.
    :return: The current block in the project.
    '''
    endkey = "###END###"


    with open(file_path, "r") as python_file:
        current_line = python_file.readline()
        beginflag = False
        nameflag = False
        category = None
        name = None

        line = 1

        content = []
        while current_line:
            if len(current_line) > 2 and current_line[:3] == "###": #TODO script doesn't work if there is a space before the #
                # This line is a keyword
                if current_line.rstrip() == endkey:
                    if beginflag and nameflag:
                        if create_structure:
                            write(python_file.name.split('/')[-1], category, name, content)
                        beginflag = False
                        nameflag = False
                        content = []
                    else:
                        raise Exception("Missing ###BEGIN###", str(line))
                else:
                    current_line = current_line.replace("###", "")
                    if "Name:" in current_line:
                        if beginflag and not nameflag:
                            name = current_line.replace("Name:", "").lstrip().rstrip()
                            value = current_setup.get(category)
                            if value == None or value == name:
                                nameflag = True
                        else:
                            raise Exception("Missing ###BEGIN###", str(line))
                    else:
                        if beginflag or nameflag:
                            raise Exception("Already saw ###BEGIN###", str(line))
                        else:
                            if current_line == "None":
                                raise Exception("Cannot use None as category name", str(line))
                            category = current_line.rstrip()
                            beginflag = True
            else:
                # This line is content
                if beginflag and nameflag:
                    content.append(current_line)
                    current_setup.update({category: name})
                elif beginflag or nameflag:
                    raise Exception("Cannot parse content, check keywords", str(line))

            line += 1
            current_line = python_file.readline()

    return current_setup

def get_setup(code_dir = "src"):
    '''
    Returns the current categories and instances in the project

    :param code_dir: directory where the project source code is.
    :return: the current categories and instances in the project
    '''

    if not os.path.exists(code_dir):
        raise Exception("Directory not found", str(1))

    current_setup = {}

    for file in os.listdir(code_dir):
        if file.endswith(".py"):
            current_setup.update( parse(code_dir+"/"+file, create_structure = False, current_setup = current_setup))

    return current_setup

def save_setup_with_score(score_file ,code_dir = "src"):
    '''
    Saves the current setup with the score metric

    :param score_file: the file where the score is.
    :param code_dir: directory where the project source code is.
    :return: current setup with the score metric
    '''
    current_setup = get_setup(code_dir)

    if not os.path.exists(score_file):
        raise Exception("Score file given is not valid: "+str(score_file))

    with open(score_file, 'r') as f: # src/output/best_accuracy.json
        data = json.load(f)
        best_acc = data['accuracy']

    branch = {}
    branch["setup"] = current_setup
    branch["score"] = best_acc

    file_path = "output/experiments_data.json"
    if os.path.exists(file_path):
        with open("output/experiments_data.json", "r") as read_file:
            content = json.load(read_file)

    else:
        content = {}
        content["experiments"] = []

    content["experiments"].append(branch)

    with open("output/experiments_data.json", "w") as write_file:
        json.dump(content, write_file, indent=2)

    return branch


def save_version(code_dir = "src"):
    '''
    Saves the current code blocks.

    :param code_dir: current setup with the score metric
    :return: None
    '''

    if not os.path.exists(code_dir):
        raise Exception("Directory not found", str(1))
    print("Files checked for categories and instances.")
    for file in os.listdir(code_dir):
        if file.endswith(".py"):
            print(file)
            parse(code_dir+"/"+file)



def use_version(component, name, code_dir = "src"):
    '''
    Does a checkout of the category and instance give.

    :param component: The category name
    :param name: The instance name 
    :param code_dir: current setup with the score metric
    :return: None
    '''

    path = "structure/"+component+"/"+name

    if not os.path.exists(path):
        raise Exception("Component and name not found in directory", str(1))

    for file in os.listdir(path):
        file_path = code_dir+"/"+file
        find_section(file_path, component, name, path+"/"+file)


if __name__ == '__main__':
    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument('-d', '--directoryname', help='directory where the code base is')
    parser.add_argument('--save', action='store_true', help="Save the current instances of the categories, "
                                             "will raise a error if there is a mistake in the one of the current files.")
    parser.add_argument('--setup', action='store_true', help="run this command to check the current categories and instances in the project "
                                                             "and saves the perfomance metric, "
                                                             "will raise a error if there is a mistake in the one of the current files.")
    parser.add_argument('-m','--metricfile', help="The file where the metric/performance is stored, setup is not saved when file is not given")
    parser.add_argument('--checkout', action='store_true', help="Use this command to checkout a instance of category, "
                                                                "the category and instance commands a required for this.")
    parser.add_argument('-c', '--category', help="The category needed")
    parser.add_argument('-i', '--instance', help="The instance needed")

    args = parser.parse_args()
    if not args.directoryname:
        raise Exception("'No directory specified", str(1))

    dir_name = args.directoryname
    if args.setup:
        if args.category or args.instance or args.checkout:
            raise Exception("Setup command can't be combined with checkout, please remove additional commands", str(1))
        elif args.metricfile:
            print('Current setup is: '+str(save_setup_with_score(args.metricfile, dir_name)))
        else:
           print('Current setup is: '+str(get_setup(dir_name)))
    elif args.checkout:
        if not args.category or not args.instance:
            raise Exception("Missing specify arguments for the checkout, please add category and/or instance", str(1))
        use_version(args.category, args.instance, dir_name)
        print("Done with checkout")
    elif args.save:
        save_version(dir_name)
        print("Done saving files")
    else:
        raise Exception("Please provide additional command.")
