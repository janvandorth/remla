"""
Download and extract data.
"""
import urllib.request
import zipfile

def main():
###Data###
###Name: v2
    URL = 'https://surfdrive.surf.nl/files/index.php/s/OZRd9BcxhGkxTuy/download' # v2
###END###

    EXTRACT_DIR = "dataset"

    zip_path, _ = urllib.request.urlretrieve(URL)
    with zipfile.ZipFile(zip_path, "r") as f:
        f.extractall(EXTRACT_DIR)


if __name__ == '__main__':
    main()
