import pytest
from text_classification import main


best_current_acc, best_previous_acc = main()
assert abs(best_current_acc - best_previous_acc) < 0.01

