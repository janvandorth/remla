# SMS Spam Detection Using Machine Learning with CBV

This project is uses the project provided by the course [*Release Engineering for Machine Learning Applications* (REMLA)] taught at the Delft University of Technology by [Prof. Luís Cruz] and [Prof. Sebastian Proksch].

The codebase was provided by : https://gitlab.com/remla-course/2021/mymodels

This repository implements the CodeBlockVersioning introduced in the paper provided as a pdf in this repo.

## Instructions running experiments

a) Clone repo.

```
$ git clone https://github.com/luiscruz/SMS-Spam-Detection.git
$ cd SMS-Spam-Detection
```

b) Install dependencies.

```
$ python -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```

c) Tagging code blocks

To make the versioning of the different categories in your code possible, you need to add key words around the specific blocks of code you want to include in the CBV.

Let's take the following code block:

```
classifiers = {
        'Random Forest': RandomForestClassifier(),
    }
```
You start with the category name and the three hastags:
The category name you want to use is 'Classifier', so you add the following.
```
###Classifier###
classifiers = {
        'Random Forest': RandomForestClassifier(),
    }
```
*make sure that there are nog spaces at the front of the hastags.

Then you want to add the specific Instance name, in this case you want to name it 'rforest'.
You add the following to above you code and below your category name.
```
###Classifier###
###Name: rforest
classifiers = {
        'Random Forest': RandomForestClassifier(),
    }
```
As the last step you add the 'END' tag.
```
###Classifier###
###Name: rforest
classifiers = {
        'Random Forest': RandomForestClassifier(),
    }
###END###
```

Now you have specified this code block within the category 'classifier' and with the instance name 'rforest'

d) Saving versions

After tagging al your code blocks you can save all the instances by one command.
Use the cbv.py file in the repository.
You need to specify where your source files are in the project, this can be done by the directory name command or short '-d'.
Because you want to save your current instance you need to use '-s'
```
$ python cbv.py -d src --save
```
*'src' is the directory name in the example above.

e) Running the DVC pipeline

Now everything has been setup, you can run the dvc pipeline.
Because the dvc.yaml has already been setup you can just run the following.
```
$ dvc repro
```

The various scripts of the cbv and machine learning tool are executed by this pipeline.
The combination of the cbv setup and performance score are saved in the 'experiments_data.json' file under the output directory.

f) Pushing to git

Now you have done your experiments your can push the results to your git repository so your teammates can see the results.


## Checking out different versions

Assuming your have tagged your code block already (if not look at step c above). 
We can create new instances of your categories and save them through the cbv commands. 

But what if you want to checkout a different instance then the one you currently have in your code base.
You can use the following command for this.
```
$ python cbv.py -d src --checkout -c Classifier -i dtree
```
The '--checkout' command is needed as well a the category name and the instance name.
In the example above the category name is 'Classifier' and the instance name is 'dtree'.
When cbv has succesfully executed this command you can run your experiment with the 'dtree' classifier in your code base. 



[*Release Engineering for Machine Learning Applications* (REMLA)]: https://se.ewi.tudelft.nl/remla/ 
[Prof. Luís Cruz]: https://luiscruz.github.io/
[Prof. Sebastian Proksch]: https://proks.ch/
